# README #

This project uses [Yarn](https://yarnpkg.com/en/) for dependencies.

To run it locally, you must install Yarn and run the following command at the repository's root to get all the dependencies.

On Debian or Ubuntu Linux, you can install Yarn via our Debian package repository. You will first need to configure the repository:

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
```

On Ubuntu 16.04 or below and Debian Stable, you will also need to configure the [NodeSource repository](https://github.com/nodesource/distributions/blob/master/README.md#deb) to get a new enough version of Node.js.

Then you can simply:

```bash
sudo apt update && sudo apt install yarn
```

If you still facing error please go to [Yarn](https://classic.yarnpkg.com/en/docs/install#debian-stable)

## Running locally for development

```bash
yarn run prep
```

Then, you can run

```bash
yarn run start
```

You can then browse to `localhost:9966` to view the application.
